/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 16:05:02 by ppeigne           #+#    #+#             */
/*   Updated: 2017/11/09 16:52:38 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t n)
{
	size_t i;
	size_t j;
	size_t lsrc;
	size_t ldst;

	lsrc = ft_strlen(src);
	ldst = ft_strlen(dst);
	if (n <= ldst)
		return (n + lsrc);
	i = ldst;
	j = 0;
	while (src[j] && j < (n - ldst - 1))
	{
		dst[i] = src[j];
		i++;
		j++;
	}
	dst[i] = '\0';
	return (ldst + lsrc);
}
