/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 17:07:46 by ppeigne           #+#    #+#             */
/*   Updated: 2017/11/16 12:31:04 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_head_spaces(const char *s)
{
	size_t	i;

	i = 0;
	while (s[i] == ' ' || s[i] == '\t' || s[i] == '\n')
		i++;
	return (i);
}

static int		ft_tail_spaces(const char *s)
{
	size_t	i;

	i = ft_strlen(s) - 1;
	while (s[i] == ' ' || s[i] == '\t' || s[i] == '\n')
		i--;
	return (i);
}

char			*ft_strtrim(char const *s)
{
	size_t	i;
	size_t	len;
	size_t	start;
	char	*str;

	if (!s)
		return (NULL);
	i = 0;
	start = ft_head_spaces(s);
	if (s[start] == '\0')
		return (ft_memalloc(1));
	len = ft_tail_spaces(s) - start + 1;
	if ((str = ft_strsub(s, start, len)) == NULL)
		return (NULL);
	return (str);
}
