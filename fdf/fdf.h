/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 17:17:51 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 18:37:53 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include "mlx.h"
# include "libft.h"
# include "get_next_line.h"
# define W 2568
# define H 1400
# define A(X) absolute_val(X)
# define I(X) img->X
# define Z_SIZE ( 2 + TILE_SIZE / 16)
# define SIZE_1 (I(t_size) * (I(x_max) - I(y_max)) < W)
# define SIZE_2 (I(t_size) * ((I(y_max) + I(x_max)) + A((I(z_min)))) / 2 < H)
# define IMG(Y, X) img->data[(I(line.Y) * W + I(line.X) + I(x_border))]
# define CD(X) mlx->img.command.X
# define X_BORDER (W - (I(t_size) * (I(x_max) - I(y_max)) + I(z_max))) / 2
# define Y_BORDER (H - (img->t_size * (img->y_max + img->x_max) / 2)) / 2

typedef struct	s_line
{
	int			x1;
	int			y1;
	int			z1;
	int			x2;
	int			y2;
	int			z2;
	int			dx;
	int			dy;
	int			swap;
	int			x_dir;
	int			y_dir;
	int			p;
}				t_line;

typedef struct	s_command
{
	int			command;
	char		*cmd;
	char		*quit;
	char		*cd_1;
	char		*cd_2;
	char		*d1;
	char		*d2;
	char		*color;
	char		*red;
	char		*green;
	char		*blue;
	char		*orange;
	char		*purple;
	char		*white;
	char		*plus;
	char		*x;
	char		*y;
	char		*d;
	char		*rot;
	char		*right;
	char		*left;
	char		*up;

}				t_command;

typedef struct	s_color
{
	int			col0;
	int			col1;
	int			col2;
}				t_color;

typedef struct	s_palette
{
	t_color		red;
	t_color		green;
	t_color		blue;
	t_color		orange;
	t_color		purple;
}				t_palette;

typedef struct	s_img
{
	t_command	command;
	int			fd;
	int			error;
	void		*img_ptr;
	int			*data;
	t_line		line;
	int			rotation;
	char		**color_arg;
	int			color_x;
	int			color_y;
	int			color_d1;
	int			color_d2;
	t_palette	palette;
	int			diag;
	int			**array_0;
	int			**array_90;
	int			**array_180;
	int			**array_270;
	int			o_x_max;
	int			o_y_max;
	int			x_max;
	int			y_max;
	int			z_min;
	int			z_max;
	int			t_size;
	int			z_size;
	int			x_border;
	int			y_border;
	int			size_l;
	int			bpp;
	int			endian;
}				t_img;

typedef struct	s_mlx
{
	void		*mlx_ptr;
	void		*win;
	t_img		img;
}				t_mlx;

int				check_sign(int nbr);
int				swap_val(int *dx, int *dy);
void			draw_line(t_img *img, int color, int i);
t_line			collect_line(t_img *img, int **array);
int				absolute_val(int nbr);
int				*atoi_tab(char **tab);
int				tab_len(char **tab);
int				file_read(t_img *img, char *av);
void			init_rotate(t_img *img);
int				**array_new(int x_size, int y_size);
int				**select_array(t_img *img);
void			size_selection(t_img *img);
void			init_color_and_lines(t_img *img);
void			check_tab(t_img *img);
void			increase_z(t_img *img, int change);
void			init_rotate(t_img *img);
void			display_y(t_img *img);
void			display_x(t_img *img);
void			display_d1(t_img *img);
void			display_d2(t_img *img);
int				f_key(int keycode, void *param);
void			color_all_plus(t_mlx *ptr);
void			color_all_minus(t_mlx *ptr);
void			rotation_plus(t_mlx *ptr);
void			rotation_minus(t_mlx *ptr);
void			rotation_180(t_mlx *ptr);
void			display_all(t_img img);
int				swap_val(int *dx, int *dy);
void			ft_swap(int *x, int *y);
void			eraser(t_mlx *ptr);
int				color_choice(int z, t_img img, int color);
void			color_for_all(t_mlx *ptr, int n);
void			red_init(t_color *red);
void			blue_init(t_color *blue);
void			green_init(t_color *green);
void			orange_init(t_color *orange);
void			purple_init(t_color *purple);
void			diag1_change(t_mlx *ptr);
void			diag2_change(t_mlx *ptr);
void			diag_adding(t_mlx *ptr);
void			x_change(t_mlx *ptr);
void			y_change(t_mlx *ptr);
void			free_char_array(char **tab);
void			free_int_array(int **tab, int len);
int				absolute_val(int nbr);
void			init_command(t_command *cd);
void			display_command(t_mlx *mlx);
void			key_cmd(t_mlx *mlx);
#endif
