/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prime_factor.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 13:49:04 by ppeigne           #+#    #+#             */
/*   Updated: 2017/11/17 13:49:08 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_prime_factor(int nb)
{
	int i;

	if (nb == 1)
		ft_putnbr(nb);
	i = 2;
	while (i <= nb)
	{
		if (ft_is_prime(i) && nb % i == 0)
		{
			ft_putnbr(i);
			if (nb / i != 1)
				ft_putchar('*');
			nb /= i;
			i = 1;
		}
		i++;
	}
	ft_putchar('\n');
	return (0);
}
