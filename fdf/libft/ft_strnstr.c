/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 14:29:51 by ppeigne           #+#    #+#             */
/*   Updated: 2017/11/15 15:15:12 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *hstk, const char *ndl, size_t n)
{
	size_t i;
	size_t j;

	i = 0;
	if (ndl[0] == '\0')
		return ((char*)hstk);
	while (hstk[i] && (i < n))
	{
		if (hstk[i] == ndl[0])
		{
			j = 0;
			while (hstk[i + j] == ndl[j] && (i + j) < n)
			{
				j++;
				if (ndl[j] == '\0')
					return ((char*)hstk + i);
			}
		}
		i++;
	}
	return (NULL);
}
