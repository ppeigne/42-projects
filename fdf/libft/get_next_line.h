/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 15:57:21 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 18:13:06 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __GET_NEXT_LINE_H
# define __GET_NEXT_LINE_H
# include "libft.h"
# include <sys/types.h>
# include <sys/uio.h>
# include <stdlib.h>
# include <unistd.h>
# define BUFF_SIZE 42
# define CAST(x) ((t_gnl*)x->content)

int					get_next_line(const int fd, char **line);

typedef struct		s_gnl
{
	char			*str;
	int				index;
	int				len;
	int				size;
}					t_gnl;
#endif
