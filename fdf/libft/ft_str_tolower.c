/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_tolower.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/16 12:06:12 by ppeigne           #+#    #+#             */
/*   Updated: 2017/11/16 12:14:29 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_str_tolower(char *str)
{
	int i;

	if (!str)
		return (NULL);
	i = 0;
	while (str[i])
	{
		ft_tolower(str[i]);
		i++;
	}
	return (str);
}
