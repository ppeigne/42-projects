/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isspace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 11:23:30 by ppeigne           #+#    #+#             */
/*   Updated: 2017/11/13 11:35:47 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_isspace(int c)
{
	if (c == '\t' || c == '\v' || c == '\n' ||
			c == '\r' || c == '\f' || c == ' ')
		return (1);
	return (0);
}
