/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/11 17:02:40 by ppeigne           #+#    #+#             */
/*   Updated: 2017/11/15 15:40:53 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int		ft_isnegative(int *n)
{
	if (*n < 0)
	{
		*n *= -1;
		return (1);
	}
	return (0);
}

char			*ft_itoa(int n)
{
	int		tmp;
	int		size;
	int		neg;
	char	*str;

	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	tmp = n;
	size = 2;
	neg = ft_isnegative(&n);
	while (tmp /= 10)
		size++;
	size += neg;
	if (!(str = (char*)malloc(sizeof(char) * size)))
		return (NULL);
	str[--size] = '\0';
	while (size--)
	{
		str[size] = n % 10 + '0';
		n = n / 10;
	}
	if (neg)
		str[0] = '-';
	return (str);
}
