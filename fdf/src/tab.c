/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/04 13:25:06 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 17:07:14 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	free_char_array(char **tab)
{
	int i;

	i = 0;
	while (tab[i])
	{
		free(tab[i]);
		i++;
	}
	free(tab);
}

void	free_int_array(int **tab, int len)
{
	int i;

	i = 0;
	while (i < len)
	{
		free(tab[i]);
		i++;
	}
	free(tab);
}

int		*atoi_tab(char **src)
{
	int len;
	int *tab;

	len = 0;
	while (src[len])
		len++;
	if (!(tab = malloc(sizeof(int) * len)))
		return (NULL);
	len--;
	while (len >= 0)
	{
		tab[len] = ft_atoi(src[len]);
		free(src[len]);
		len--;
	}
	free(src);
	return (tab);
}

int		tab_len(char **tab)
{
	int len;

	len = 0;
	while (tab[len] != 0)
		len++;
	return (len);
}

void	check_tab(t_img *img)
{
	int i;
	int j;

	img->z_min = 0;
	img->z_max = 0;
	i = 0;
	while (i < img->y_max - 1)
	{
		j = 0;
		while (j < img->x_max - 1)
		{
			if (img->array_0[i][j] > img->z_max)
				img->z_max = img->array_0[i][j];
			if (img->array_0[i][j] < img->z_min)
				img->z_min = img->array_0[i][j];
			j++;
		}
		i++;
	}
}
