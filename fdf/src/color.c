/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 15:15:08 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/11 15:03:55 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	red_init(t_color *red)
{
	red->col0 = 0xB71C1C;
	red->col1 = 0xF44336;
	red->col2 = 0xFF7043;
}

void	blue_init(t_color *blue)
{
	blue->col0 = 0x1A237E;
	blue->col1 = 0x039BE5;
	blue->col2 = 0x80DEEA;
}

void	green_init(t_color *green)
{
	green->col0 = 0x1B5E20;
	green->col1 = 0x4CAF50;
	green->col2 = 0x9CCC65;
}

void	orange_init(t_color *orange)
{
	orange->col0 = 0xBF360C;
	orange->col1 = 0xEF6C00;
	orange->col2 = 0xFFB300;
}

void	purple_init(t_color *purple)
{
	purple->col0 = 0x4A148C;
	purple->col1 = 0xAB47BC;
	purple->col2 = 0x9FA8DA;
}
