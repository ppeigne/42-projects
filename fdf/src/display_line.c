/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 13:43:48 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 16:34:15 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	display_x(t_img *img)
{
	int x;
	int y;
	int **coor;

	coor = select_array(img);
	y = img->y_max - 1;
	while (y >= 0)
	{
		x = img->x_max - 2;
		while (x >= 0)
		{
			img->line.x1 = x;
			img->line.y1 = y;
			img->line.x2 = x + 1;
			img->line.y2 = y;
			img->line = collect_line(img, coor);
			draw_line(img, img->color_x, 0);
			x--;
		}
		y--;
	}
}

void	display_y(t_img *img)
{
	int x;
	int y;
	int **coor;

	coor = select_array(img);
	x = img->x_max - 1;
	while (x >= 0)
	{
		y = img->y_max - 2;
		while (y >= 0)
		{
			img->line.x1 = x;
			img->line.y1 = y;
			img->line.x2 = x;
			img->line.y2 = y + 1;
			img->line = collect_line(img, coor);
			draw_line(img, img->color_y, 0);
			y--;
		}
		x--;
	}
}

void	display_d1(t_img *img)
{
	int x;
	int y;
	int **coor;

	if (img->diag != 1 && img->diag != 3)
		img->color_d1 = -2;
	coor = select_array(img);
	x = img->x_max - 2;
	while (x >= 0)
	{
		y = img->y_max - 2;
		while (y >= 0)
		{
			img->line.x1 = x;
			img->line.y1 = y;
			img->line.x2 = x + 1;
			img->line.y2 = y + 1;
			img->line = collect_line(img, coor);
			draw_line(img, img->color_d1, 0);
			y--;
		}
		x--;
	}
}

void	display_d2(t_img *img)
{
	int x;
	int y;
	int **coor;

	if (img->diag != 2 && img->diag != 3)
		img->color_d2 = -2;
	coor = select_array(img);
	x = img->x_max - 2;
	while (x >= 0)
	{
		y = img->y_max - 1;
		while (y > 0)
		{
			img->line.x1 = x;
			img->line.y1 = y;
			img->line.x2 = x + 1;
			img->line.y2 = y - 1;
			img->line = collect_line(img, coor);
			draw_line(img, img->color_d2, 0);
			y--;
		}
		x--;
	}
}

void	display_all(t_img img)
{
	display_d1(&img);
	display_d2(&img);
	display_x(&img);
	display_y(&img);
}
