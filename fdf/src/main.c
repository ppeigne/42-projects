/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 18:33:04 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 18:33:07 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	init_command(t_command *cd)
{
	cd->cmd = "COMMAND:";
	cd->quit = "Press [ESC] or [Q] for quit";
	cd->cd_1 = "Press [C] to remove command instructions";
	cd->cd_2 = "Press [C] to display command instructions";
	cd->d1 = "DIAGONAL:";
	cd->d2 = "Press [D] to display diagonal lines";
	cd->color = "COLOR:";
	cd->red = "Press [R] for red";
	cd->green = "Press [G] for green";
	cd->blue = "Press [B] for blue";
	cd->orange = "Press [O] for orange";
	cd->purple = "Press [P] for purple";
	cd->white = "Press [W] for white";
	cd->plus = "Press [+] or [-] to change overall color";
	cd->x = "Press [X] to change color of X lines";
	cd->y = "Press [Y] to change color of Y lines";
	cd->d = "Press [V] or [H] to change color of diagonal lines";
	cd->rot = "ROTATION";
	cd->right = "Press [RIGHT] for +90 degree rotation";
	cd->left = "Press [LEFT] for -90 degree rotation";
	cd->up = "Press [UP] or [DOWN] for 180 degree rotation";
}

void	display_part_cmd(t_mlx *mlx)
{
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 135, 0x00FFFFFF, CD(color));
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 155, 0x00FFFFFF, CD(red));
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 175, 0x00FFFFFF, CD(green));
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 195, 0x00FFFFFF, CD(blue));
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 215, 0x00FFFFFF, CD(orange));
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 235, 0x00FFFFFF, CD(purple));
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 255, 0x00FFFFFF, CD(white));
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 275, 0x00FFFFFF, CD(plus));
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 295, 0x00FFFFFF, CD(x));
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 315, 0x00FFFFFF, CD(y));
	mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 335, 0x00FFFFFF, CD(d));
}

void	display_command(t_mlx *mlx)
{
	if (CD(command) == 1)
	{
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 5, 0x00FFFFFF, CD(cmd));
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 25, 0x00FFFFFF, CD(quit));
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 45, 0x00FFFFFF, CD(cd_1));
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 80, 0x00FFFFFF, CD(d1));
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 100, 0x00FFFFFF, CD(d2));
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 370, 0x00FFFFFF, CD(rot));
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 390, 0x00FFFFFF, CD(right));
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 410, 0x00FFFFFF, CD(left));
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 430, 0x00FFFFFF, CD(up));
		display_part_cmd(mlx);
	}
	else
	{
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 5, 0x00FFFFFF, CD(cmd));
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 25, 0x00FFFFFF, CD(quit));
		mlx_string_put(mlx->mlx_ptr, mlx->win, 5, 45, 0x00FFFFFF, CD(cd_2));
	}
}

void	check_arg(int ac, char *av, t_mlx *ptr)
{
	int		i;
	char	*file_type;

	if (ac != 2)
	{
		ft_putendl("usage : ./fdf [file.fdf]");
		exit(1);
	}
	i = ft_strlen(av) - 1;
	file_type = ".fdf";
	if (ft_strcmp(&av[i - 3], file_type) || (!file_read(&ptr->img, av)))
	{
		ft_putendl("invalid file");
		close(ptr->img.fd);
		exit(1);
	}
}

int		main(int ac, char **av)
{
	t_mlx	mlx;

	mlx.img.fd = open(av[1], O_RDONLY);
	check_arg(ac, av[1], &mlx);
	mlx.mlx_ptr = mlx_init();
	mlx.win = mlx_new_window(mlx.mlx_ptr, W, H, "Fdf");
	mlx.img.img_ptr = mlx_new_image(mlx.mlx_ptr, W, H);
	mlx.img.data = (int *)mlx_get_data_addr(mlx.img.img_ptr, &mlx.img.bpp,
			&mlx.img.size_l, &mlx.img.endian);
	init_color_and_lines(&mlx.img);
	size_selection(&mlx.img);
	mlx_key_hook(mlx.win, f_key, (void*)&mlx);
	display_all(mlx.img);
	mlx_put_image_to_window(mlx.mlx_ptr, mlx.win, mlx.img.img_ptr, 0, 0);
	display_command(&mlx);
	mlx_loop(mlx.mlx_ptr);
	close(mlx.img.fd);
	return (0);
}
