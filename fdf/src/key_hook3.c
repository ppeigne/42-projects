/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook3.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/11 14:36:25 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 17:05:07 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	diag1_change(t_mlx *ptr)
{
	ptr->img.color_d1 += 1;
	if (ptr->img.color_d1 > 4)
		ptr->img.color_d1 = 0;
	mlx_clear_window(ptr->mlx_ptr, ptr->win);
	display_d1(&ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}

void	diag2_change(t_mlx *ptr)
{
	ptr->img.color_d2 += 1;
	if (ptr->img.color_d2 > 4)
		ptr->img.color_d2 = 0;
	mlx_clear_window(ptr->mlx_ptr, ptr->win);
	display_d2(&ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}

void	diag_adding(t_mlx *ptr)
{
	ptr->img.diag += 1;
	if (ptr->img.diag > 3)
		ptr->img.diag = 0;
	mlx_clear_window(ptr->mlx_ptr, ptr->win);
	display_all(ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}

void	x_change(t_mlx *ptr)
{
	ptr->img.color_x += 1;
	if (ptr->img.color_x > 4)
		ptr->img.color_x = 0;
	display_x(&ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}

void	y_change(t_mlx *ptr)
{
	ptr->img.color_y += 1;
	if (ptr->img.color_y > 4)
		ptr->img.color_y = 0;
	display_y(&ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}
