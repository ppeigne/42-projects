/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/11 13:54:17 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 17:45:24 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		**select_array(t_img *img)
{
	if (img->rotation == 0)
		return (img->array_0);
	else if (img->rotation == 1)
		return (img->array_90);
	else if (img->rotation == 2)
		return (img->array_180);
	else if (img->rotation == 3)
		return (img->array_270);
	else
	{
		img->error = 3;
		return (NULL);
	}
}

void	size_selection(t_img *img)
{
	img->t_size = 2;
	while (SIZE_1 && SIZE_2 && img->t_size < 64)
		img->t_size++;
	img->t_size--;
	if (img->t_size > 12)
		img->t_size -= 2;
	if (img->x_max == img->y_max)
		img->t_size--;
	img->x_border = X_BORDER;
	img->y_border = Y_BORDER;
	img->z_size = 2 + (img->z_max + absolute_val(img->z_min)) / img->t_size;
}

t_line	collect_line(t_img *img, int **array)
{
	int x1;
	int y1;
	int x2;
	int	y2;

	x1 = img->line.x1;
	y1 = img->line.y1;
	img->line.z1 = array[y1][x1];
	x2 = img->line.x2;
	y2 = img->line.y2;
	img->line.z2 = array[y2][x2];
	img->line.x1 = (x1 - y1) * img->t_size;
	img->line.y1 = (x1 + y1) * (img->t_size / 2)
		- (array[y1][x1] * img->z_size) + img->y_border;
	img->line.x2 = (x2 - y2) * img->t_size;
	img->line.y2 = (x2 + y2) * (img->t_size / 2)
		- (array[y2][x2] * img->z_size) + img->y_border;
	img->line.dx = absolute_val(img->line.x2 - img->line.x1);
	img->line.dy = absolute_val(img->line.y2 - img->line.y1);
	img->line.x_dir = check_sign(img->line.x2 - img->line.x1);
	img->line.y_dir = check_sign(img->line.y2 - img->line.y1);
	img->line.swap = swap_val(&img->line.dx, &img->line.dy);
	img->line.p = (2 * img->line.dy) - img->line.dx;
	return (img->line);
}

int		color_display(int z, t_color color, int z_min, int z_max)
{
	if (z < z_min + ((z_max - z_min) * 3 / 10))
		return (color.col0);
	if (z < z_min + ((z_max - z_min) * 6 / 10))
		return (color.col1);
	else
		return (color.col2);
}

int		color_choice(int z, t_img img, int color)
{
	if (color == -2)
		return (0);
	else if (color == -1)
		return (0xFFFFFF);
	else if (color == 0)
		return (color_display(z, img.palette.red, img.z_min, img.z_max));
	else if (color == 1)
		return (color_display(z, img.palette.green, img.z_min, img.z_max));
	else if (color == 2)
		return (color_display(z, img.palette.blue, img.z_min, img.z_max));
	else if (color == 3)
		return (color_display(z, img.palette.orange, img.z_min, img.z_max));
	else
		return (color_display(z, img.palette.purple, img.z_min, img.z_max));
}
