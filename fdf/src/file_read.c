/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_read.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/02 16:53:31 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 20:07:23 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		lines_count(t_img *img, char *av)
{
	char **tab;
	char *line;

	get_next_line(img->fd, &line);
	tab = ft_strsplit(line, ' ');
	img->x_max = tab_len(tab);
	img->y_max = 1;
	free(line);
	free_char_array(tab);
	while (get_next_line(img->fd, &line))
	{
		img->y_max++;
		tab = ft_strsplit(line, ' ');
		if (tab_len(tab) != img->x_max)
			return (0);
		img->x_max = tab_len(tab);
		free_char_array(tab);
		free(line);
	}
	close(img->fd);
	free(line);
	img->fd = open(av, O_RDONLY);
	return (1);
}

int		**array_new(int x_size, int y_size)
{
	int **array;
	int	i;

	i = 0;
	if (!(array = (int**)malloc(sizeof(int*) * (y_size))))
		return (NULL);
	while (i < y_size)
	{
		if (!(array[i] = (int*)malloc(sizeof(int) * (x_size))))
		{
			free_int_array(array, i);
			return (NULL);
		}
		i++;
	}
	return (array);
}

int		creating_array_0(t_img *img)
{
	char	*line;
	int		*tab;
	int		i;
	int		j;

	i = 0;
	while (get_next_line(img->fd, &line) > 0)
	{
		tab = atoi_tab(ft_strsplit(line, ' '));
		j = 0;
		while (j < img->x_max)
		{
			img->array_0[i][j] = tab[j];
			j++;
		}
		free(tab);
		free(line);
		i++;
	}
	free(line);
	close(img->fd);
	return (1);
}

int		file_read(t_img *img, char *av)
{
	if (!lines_count(img, av))
		return (0);
	if (!(img->array_0 = array_new(img->x_max, img->y_max)))
		return (0);
	if (!(creating_array_0(img)))
		return (0);
	if (img->y_max <= 1 || img->x_max <= 1)
		return (0);
	img->o_x_max = img->x_max;
	img->o_y_max = img->y_max;
	init_rotate(img);
	check_tab(img);
	return (1);
}
