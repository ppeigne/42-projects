/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/03 14:07:25 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 17:08:22 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	rotate_90(t_img *img)
{
	int x;
	int y;

	y = 0;
	img->array_90 = array_new(img->y_max, img->x_max);
	while (y < img->y_max)
	{
		x = 0;
		while (x < img->x_max)
		{
			img->array_90[x][img->y_max - y - 1] = img->array_0[y][x];
			x++;
		}
		y++;
	}
}

void	rotate_180(t_img *img)
{
	int x;
	int y;

	y = 0;
	img->array_180 = array_new(img->x_max, img->y_max);
	while (y < img->y_max)
	{
		x = 0;
		while (x < img->x_max)
		{
			img->array_180[img->y_max - y - 1]
				[img->x_max - x - 1] = img->array_0[y][x];
			x++;
		}
		y++;
	}
}

void	rotate_270(t_img *img)
{
	int x;
	int y;

	y = 0;
	img->array_270 = array_new(img->y_max, img->x_max);
	while (y < img->y_max)
	{
		x = 0;
		while (x < img->x_max)
		{
			img->array_270[img->x_max - x - 1][y] = img->array_0[y][x];
			x++;
		}
		y++;
	}
}

void	init_rotate(t_img *img)
{
	rotate_90(img);
	rotate_180(img);
	rotate_270(img);
}
