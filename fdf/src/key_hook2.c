/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 14:15:03 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 16:46:19 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	color_all_plus(t_mlx *ptr)
{
	ptr->img.color_x += 1;
	ptr->img.color_y += 1;
	ptr->img.color_d1 += 1;
	ptr->img.color_d2 += 1;
	if (ptr->img.color_x > 4)
		ptr->img.color_x = 0;
	if (ptr->img.color_y > 4)
		ptr->img.color_y = 0;
	if (ptr->img.color_d1 > 4)
		ptr->img.color_d1 = 0;
	if (ptr->img.color_d2 > 4)
		ptr->img.color_d2 = 0;
	eraser(ptr);
	display_all(ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}

void	color_all_minus(t_mlx *ptr)
{
	ptr->img.color_x -= 1;
	ptr->img.color_y -= 1;
	ptr->img.color_d1 -= 1;
	ptr->img.color_d2 -= 1;
	if (ptr->img.color_x < 0)
		ptr->img.color_x = 4;
	if (ptr->img.color_y < 0)
		ptr->img.color_y = 4;
	if (ptr->img.color_d1 < 0)
		ptr->img.color_d1 = 4;
	if (ptr->img.color_d2 < 0)
		ptr->img.color_d2 = 4;
	eraser(ptr);
	display_all(ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}

void	rotation_plus(t_mlx *ptr)
{
	eraser(ptr);
	ptr->img.rotation += 1;
	if (ptr->img.rotation > 3)
		ptr->img.rotation = 0;
	ft_swap(&ptr->img.x_max, &ptr->img.y_max);
	size_selection(&ptr->img);
	display_all(ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}

void	rotation_minus(t_mlx *ptr)
{
	eraser(ptr);
	ptr->img.rotation -= 1;
	if (ptr->img.rotation < 0)
		ptr->img.rotation = 3;
	ft_swap(&ptr->img.x_max, &ptr->img.y_max);
	size_selection(&ptr->img);
	display_all(ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}

void	rotation_180(t_mlx *ptr)
{
	eraser(ptr);
	ptr->img.rotation += 2;
	if (ptr->img.rotation > 3)
		ptr->img.rotation -= 4;
	display_all(ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}
