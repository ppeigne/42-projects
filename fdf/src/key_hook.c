/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 11:18:10 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 16:43:51 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	exit_window(t_mlx *ptr)
{
	free_int_array(ptr->img.array_0, ptr->img.o_y_max);
	free_int_array(ptr->img.array_90, ptr->img.o_x_max);
	free_int_array(ptr->img.array_180, ptr->img.o_y_max);
	free_int_array(ptr->img.array_270, ptr->img.o_x_max);
	free(ptr->img.data);
	exit(1);
}

void	key_color_all(int keycode, t_mlx *ptr)
{
	if (keycode == 69 || keycode == 24)
		color_all_plus(ptr);
	else if (keycode == 78 || keycode == 27)
		color_all_minus(ptr);
	else if (keycode == 15)
		color_for_all(ptr, 0);
	else if (keycode == 5)
		color_for_all(ptr, 1);
	else if (keycode == 11)
		color_for_all(ptr, 2);
	else if (keycode == 31)
		color_for_all(ptr, 3);
	else if (keycode == 35)
		color_for_all(ptr, 4);
	else if (keycode == 13)
		color_for_all(ptr, -1);
}

void	key_color_independent(int keycode, t_mlx *ptr)
{
	if (keycode == 7)
		x_change(ptr);
	else if (keycode == 16)
		y_change(ptr);
	else if (keycode == 2)
		diag_adding(ptr);
	else if (keycode == 9)
		diag1_change(ptr);
	else if (keycode == 4)
		diag2_change(ptr);
}

void	eraser(t_mlx *ptr)
{
	int color_x;
	int color_y;
	int color_d1;
	int color_d2;

	color_x = ptr->img.color_x;
	color_y = ptr->img.color_y;
	color_d1 = ptr->img.color_d1;
	color_d2 = ptr->img.color_d2;
	ptr->img.color_x = -2;
	ptr->img.color_y = -2;
	ptr->img.color_d1 = -2;
	ptr->img.color_d2 = -2;
	display_all(ptr->img);
	ptr->img.color_x = color_x;
	ptr->img.color_y = color_y;
	ptr->img.color_d1 = color_d1;
	ptr->img.color_d2 = color_d2;
}

int		f_key(int k, void *param)
{
	t_mlx *ptr;

	ptr = (t_mlx*)param;
	if (k == 53 || k == 12)
		exit_window(ptr);
	else if (k == 5 || k == 11 || k == 13 || k == 15 || k == 24
			|| k == 27 || k == 31 || k == 35 || k == 69 || k == 78)
		key_color_all(k, ptr);
	else if (k == 2 || k == 4 || k == 7 || k == 9 || k == 16)
		key_color_independent(k, ptr);
	else if (k == 124)
		rotation_plus(ptr);
	else if (k == 123)
		rotation_minus(ptr);
	else if (k == 125 || k == 126)
		rotation_180(ptr);
	else if (k == 8)
		key_cmd(ptr);
	return (0);
}
