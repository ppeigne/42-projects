/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/31 14:34:25 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 16:57:54 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		check_sign(int nbr)
{
	if (nbr > 0)
		return (1);
	else if (nbr < 0)
		return (-1);
	else
		return (0);
}

int		absolute_val(int nbr)
{
	if (nbr < 0)
		nbr *= -1;
	return (nbr);
}

int		swap_val(int *dx, int *dy)
{
	if (*dy > *dx)
	{
		ft_swap(dx, dy);
		return (1);
	}
	return (0);
}

void	ft_swap(int *x, int *y)
{
	int tmp;

	tmp = *x;
	*x = *y;
	*y = tmp;
}

void	draw_line(t_img *img, int color, int i)
{
	IMG(y1, x1) = color_choice(img->line.z1, *img, color);
	while (i < img->line.dx)
	{
		IMG(y1, x1) = color_choice((img->line.z1 + img->line.z2) / 2,
				*img, color);
		while (img->line.p >= 0)
		{
			img->line.p = img->line.p - (2 * img->line.dx);
			if (img->line.swap)
				img->line.x1 += img->line.x_dir;
			else
				img->line.y1 += img->line.y_dir;
		}
		img->line.p = img->line.p + (2 * img->line.dy);
		if (img->line.swap)
			img->line.y1 += img->line.y_dir;
		else
			img->line.x1 += img->line.x_dir;
		i++;
	}
	IMG(y2, x2) = color_choice(img->line.z2, *img, color);
}
