/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook_color.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 16:17:13 by ppeigne           #+#    #+#             */
/*   Updated: 2018/01/19 17:04:50 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	color_for_all(t_mlx *ptr, int n)
{
	ptr->img.color_x = n;
	ptr->img.color_y = n;
	ptr->img.color_d1 = n;
	ptr->img.color_d2 = n;
	display_all(ptr->img);
	mlx_put_image_to_window(ptr->mlx_ptr, ptr->win, ptr->img.img_ptr, 0, 0);
	display_command(ptr);
}

void	init_color_and_lines(t_img *img)
{
	red_init(&img->palette.red);
	blue_init(&img->palette.blue);
	green_init(&img->palette.green);
	orange_init(&img->palette.orange);
	purple_init(&img->palette.purple);
	img->color_x = 0;
	img->color_y = 0;
	img->color_d1 = 0;
	img->color_d2 = 0;
	img->diag = 0;
	img->rotation = 0;
	img->command.command = 1;
	init_command(&img->command);
}

void	key_cmd(t_mlx *mlx)
{
	if (CD(command) == 1)
		CD(command) = 0;
	else if (CD(command) == 0)
		CD(command) = 1;
	mlx_clear_window(mlx->mlx_ptr, mlx->win);
	display_all(mlx->img);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, mlx->img.img_ptr, 0, 0);
	display_command(mlx);
}
